<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ruler Entity
 *
 * @property int $id
 * @property int|null $sequence
 * @property string|null $ruler
 * @property int|null $period_id
 * @property int|null $dynasty_id
 *
 * @property \App\Model\Entity\Period $period
 * @property \App\Model\Entity\Dynasty $dynasty
 * @property \App\Model\Entity\Date[] $dates
 */
class Ruler extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sequence' => true,
        'ruler' => true,
        'period_id' => true,
        'dynasty_id' => true,
        'period' => true,
        'dynasty' => true,
        'dates' => true
    ];
}
