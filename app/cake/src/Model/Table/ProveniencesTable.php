<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Proveniences Model
 *
 * @property \App\Model\Table\RegionsTable|\Cake\ORM\Association\BelongsTo $Regions
 * @property \App\Model\Table\ArchivesTable|\Cake\ORM\Association\HasMany $Archives
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\HasMany $Artifacts
 * @property \App\Model\Table\DynastiesTable|\Cake\ORM\Association\HasMany $Dynasties
 *
 * @method \App\Model\Entity\Provenience get($primaryKey, $options = [])
 * @method \App\Model\Entity\Provenience newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Provenience[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Provenience|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Provenience|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Provenience patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Provenience[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Provenience findOrCreate($search, callable $callback = null, $options = [])
 */
class ProveniencesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('proveniences');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Regions', [
            'foreignKey' => 'region_id'
        ]);
        $this->hasMany('Archives', [
            'foreignKey' => 'provenience_id'
        ]);
        $this->hasMany('Artifacts', [
            'foreignKey' => 'provenience_id'
        ]);
        $this->hasMany('Dynasties', [
            'foreignKey' => 'provenience_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('provenience')
            ->maxLength('provenience', 100)
            ->allowEmpty('provenience');

        $validator
            ->scalar('geo_coordinates')
            ->maxLength('geo_coordinates', 50)
            ->allowEmpty('geo_coordinates');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['region_id'], 'Regions'));

        return $rules;
    }
}
