<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Edit User') ?></div>

        <?= $this->Flash->render() ?>

        <?= $this->Form->create($user) ?>
            <fieldset>
                <?php
                    echo $this->Form->control('username', ['class' => 'form-control']);
                    echo $this->Form->control('email', ['class' => 'form-control']);
                    echo $this->Form->control('password', ['autocomplete' => 'off', 'class' => 'form-control', 'default' => 'a']);
                ?>
            </fieldset>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('View your profile'), ['action' => 'view'], ['class' => 'btn-action']) ?>
    </div>

</div>
