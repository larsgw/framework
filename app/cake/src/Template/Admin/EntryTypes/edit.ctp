<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\EntryType $entryType
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($entryType) ?>
            <legend class="capital-heading"><?= __('Edit Entry Type') ?></legend>
            <?php
                echo $this->Form->control('label');
                echo $this->Form->control('author', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('title', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('journal', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('year', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('volume', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('pages', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('number', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('month', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('eid', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('note', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('crossref', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('keyword', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('doi', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('url', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('file', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('citeseerurl', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('pdf', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('abstract', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('comment', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('owner', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('timestamp', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('review', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('search', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('publisher', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('editor', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('series', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('address', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('edition', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('howpublished', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('lastchecked', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('booktitle', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('organization', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('language', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('chapter', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('type', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('school', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('nationality', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('yearfiled', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('assignee', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('day', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('dayfiled', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('monthfiled', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('institution', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('revision', ['options' => $entryTypes, 'empty' => true]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $entryType->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $entryType->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Entry Types'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
