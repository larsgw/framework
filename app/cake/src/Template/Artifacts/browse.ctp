<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */
?>
<h1>Browse</h1>

<div align="left">
	<ul>
		<li><a href="/abbreviations">Abbreviations</a></li>
		<li><a href="/archives">Archives</a></li>
		<li><a href="/artifacts">Artifacts</a></li>
		<li><a href="/artifactTypes">Artifact Types</a></li>
		<li><a href="/authors">Authors</a></li>
		<li><a href="/collections">Collections</a></li>
		<li><a href="/dates">Dates</a></li>
		<li><a href="/dynasties">Dynasties</a></li>
		<li><a href="/externalResources">ExternalResources</a></li>
		<li><a href="/genres">Genres</a></li>
		<li><a href="/journals">Journals</a></li>
		<li><a href="/languages">Languages</a></li>
		<li><a href="/materialAspects">MaterialAspects</a></li>
		<li><a href="/materialColors">MaterialColors</a></li>
		<li><a href="/materials">Materials</a></li>
		<li><a href="/periods">Periods</a></li>
		<li><a href="/postings">Postings</a></li>
		<li><a href="/proveniences">Proveniences</a></li>
		<li><a href="/publications">Publications</a></li>
		<li><a href="/regions">Regions</a></li>
		<li><a href="/rulers">Rulers</a></li>
		<li><a href="/signReadings">SignReadings</a></li>
		<li><a href="/users">Users</a></li>
	</ul>
</div>

