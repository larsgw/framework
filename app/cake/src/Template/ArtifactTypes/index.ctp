<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactType[]|\Cake\Collection\CollectionInterface $artifactTypes
 */
?>


<h3 class="display-4 pt-3"><?= __('Artifact Types') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('artifact_type') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
            <!-- <th scope="col"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactTypes as $artifactType): ?>
        <tr>
            <td><a href="/artifactTypes/<?= h($artifactType->id) ?>"><?= h($artifactType->artifact_type) ?></a></td>
            <td><?= $artifactType->has('parent_artifact_type') ? $this->Html->link($artifactType->parent_artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifactType->parent_artifact_type->id]) : '' ?></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $artifactType->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactType->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactType->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactType->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

