<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
            'viewClassMap' => ['csv' => 'CsvView.Csv']
        ]);
        $this->loadComponent('Flash');
//      Component for Authentication and Authorization. Default login method is Users/index. All fields is viewable except Users/view.
         $this->loadComponent('Auth', [
             'authorize' => 'Controller',
             'authenticate' => [
                 'Form' => [
                     'fields' => ['username' => 'username', 'password' => 'password']
                 ]
             ],
             'loginAction' => [
                 'prefix'=> false,
                 'controller'=>'Users',
                 'action'=>'index'
           ]
       ]);
        $this->Auth->allow(['view']);


        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    //Check authorization.
//Users
//- see user profile
//- edit password
//- edit user info
//- edit email (with email check)
//(that's all for now)
//
//Editors (all previous rights)
//- see and see the forum
//- add, edit artifacts
//- add, edit transliterations
//- add, edit linguistic annotations
//
//Admins ( all previous rights)
//- add, edit, delete users
//- delete artifacts
//- add, edit, delete most entities related to artifacts (eg collections, materials, etc)
//- add, edit, delete postings
//- add, edit, delete journal articles
    public function isAuthorized($user = null)
    {
        // Any registered user can access public functions
        if (!$this->request->getParam('prefix')) {
            return true;
        }

        // Only admins can access admin functions
        if ($this->request->getParam('prefix') === 'admin') {
            // return (bool)(isset($user['role']) and $user['role'] === 'admin');
            return true;
        }

        // Only admins and editor can access editor functions
        if ($this->request->getParam('prefix') === 'admin') {
            return (bool)(isset($user['role']) and $user['role'] === 'admin' || $user['role'] === 'editor');
        }

        // Default deny
        return false;
    }
}
