<?php
namespace App\Controller;

use App\Controller\AppController;
use GoogleAuthenticator\GoogleAuthenticator;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TwofactorloginController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Authors', 'Credits', 'Inscriptions']
        ]);

        $this->set('user', $user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadComponent('GoogleAuthenticator');
        $ga = $this->GoogleAuthenticator;
        $user = $this->getRequest()->getSession()->read('User');

        if ($this->request->is('post')) {

            //check if login available
            if ($user) {
                $secret = $user['2fa_key'];
                $oneCode = $this->request->data['code'];
                $checkResult = $ga->verifyCode($secret, $oneCode, 2);

                if ($checkResult) {
                    $this->Auth->setUser($user);
                    return $this->redirect([
                        'controller' => 'Search',
                        'action' => 'index'
                    ]);
                } else {
                    $this->Flash->error(__('Wrong code entered.Please try again.'),array('class' => 'alert alert-danger'));
                }
            }

            else {
                $this->Flash->error(__('Username or password is incorrect'));
                return $this->redirect(['controller' => 'Users', 'action' => 'index']);
            }
        }
    }

}
