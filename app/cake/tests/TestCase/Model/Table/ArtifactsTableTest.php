<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArtifactsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArtifactsTable Test Case
 */
class ArtifactsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ArtifactsTable
     */
    public $Artifacts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.artifacts',
        'app.credits',
        'app.proveniences',
        'app.periods',
        'app.artifact_types',
        'app.archives',
        'app.artifact_dates',
        'app.artifacts_composites',
        'app.artifacts_date_referenced',
        'app.artifacts_seals',
        'app.artifacts_shadow',
        'app.inscriptions',
        'app.retired_artifacts',
        'app.collections',
        'app.dates',
        'app.external_resources',
        'app.genres',
        'app.languages',
        'app.materials',
        'app.publications'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Artifacts') ? [] : ['className' => ArtifactsTable::class];
        $this->Artifacts = TableRegistry::getTableLocator()->get('Artifacts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Artifacts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getNumberOfRowsInTable method
     *
     * @return void
     */
    public function testGetNumberOfRowsInTable()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getNumberOfNullValuesByColumn method
     *
     * @return void
     */
    public function testGetNumberOfNullValuesByColumn()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getNumberOfDistinctValuesByColumn method
     *
     * @return void
     */
    public function testGetNumberOfDistinctValuesByColumn()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getMaximumValueByColumn method
     *
     * @return void
     */
    public function testGetMaximumValueByColumn()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getMinimumValueByColumn method
     *
     * @return void
     */
    public function testGetMinimumValueByColumn()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getLanguagesWithCount method
     *
     * @return void
     */
    public function testGetLanguagesWithCount()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test processData method
     *
     * @return void
     */
    public function testProcessData()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getTopGenres method
     *
     * @return void
     */
    public function testGetTopGenres()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getTopLanguages method
     *
     * @return void
     */
    public function testGetTopLanguages()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getTopMaterials method
     *
     * @return void
     */
    public function testGetTopMaterials()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getTopRegions method
     *
     * @return void
     */
    public function testGetTopRegions()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getRadarChartData method
     *
     * @return void
     */
    public function testGetRadarChartData()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getRadarChartLegend method
     *
     * @return void
     */
    public function testGetRadarChartLegend()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getRegionByArtifactId method
     *
     * @return void
     */
    public function testGetRegionByArtifactId()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getProvenienceByArtifactId method
     *
     * @return void
     */
    public function testGetProvenienceByArtifactId()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getRegionIdMappingByProvenienceId method
     *
     * @return void
     */
    public function testGetRegionIdMappingByProvenienceId()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getRegionMappingByRegionId method
     *
     * @return void
     */
    public function testGetRegionMappingByRegionId()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test formatRadarChartData method
     *
     * @return void
     */
    public function testFormatRadarChartData()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getLineChartData method
     *
     * @return void
     */
    public function testGetLineChartData()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getPeriodLanguageLineChart method
     *
     * @return void
     */
    public function testGetPeriodLanguageLineChart()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getPeriodMaterialLineChart method
     *
     * @return void
     */
    public function testGetPeriodMaterialLineChart()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getPeriodGenreLineChart method
     *
     * @return void
     */
    public function testGetPeriodGenreLineChart()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getSequenceToPeriodMapping method
     *
     * @return void
     */
    public function testGetSequenceToPeriodMapping()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getPeriodIdToSequenceMapping method
     *
     * @return void
     */
    public function testGetPeriodIdToSequenceMapping()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getDendrogramChartData method
     *
     * @return void
     */
    public function testGetDendrogramChartData()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getMaterialParents method
     *
     * @return void
     */
    public function testGetMaterialParents()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getMaterialIdToMaterialMapping method
     *
     * @return void
     */
    public function testGetMaterialIdToMaterialMapping()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getChoroplethMapData method
     *
     * @return void
     */
    public function testGetChoroplethMapData()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getChoroplethMapForProveniences method
     *
     * @return void
     */
    public function testGetChoroplethMapForProveniences()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getChoroplethMapForRegions method
     *
     * @return void
     */
    public function testGetChoroplethMapForRegions()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test formatRegionCoordinates method
     *
     * @return void
     */
    public function testFormatRegionCoordinates()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
