<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DatesFixture
 *
 */
class DatesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'day_no' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'day_remarks' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'month_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'is_uncertain' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'month_no' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'year_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dynasty_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ruler_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'absolute_year' => ['type' => 'string', 'length' => 15, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'fk_dates_months_month_id' => ['type' => 'index', 'columns' => ['month_id'], 'length' => []],
            'fk_dates_years_year_id' => ['type' => 'index', 'columns' => ['year_id'], 'length' => []],
            'fk_dates_dynasty_dynasty_id' => ['type' => 'index', 'columns' => ['dynasty_id'], 'length' => []],
            'fk_dates_rulers_ruler_id' => ['type' => 'index', 'columns' => ['ruler_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'unique_date_idx' => ['type' => 'unique', 'columns' => ['day_no', 'month_no', 'year_id'], 'length' => []],
            'fk_dates_dynasty_dynasty_id' => ['type' => 'foreign', 'columns' => ['dynasty_id'], 'references' => ['dynasties', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_dates_months_month_id' => ['type' => 'foreign', 'columns' => ['month_id'], 'references' => ['months', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_dates_rulers_ruler_id' => ['type' => 'foreign', 'columns' => ['ruler_id'], 'references' => ['rulers', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_dates_years_year_id' => ['type' => 'foreign', 'columns' => ['year_id'], 'references' => ['years', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'day_no' => 'Lorem ipsum dolor sit amet',
                'day_remarks' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'month_id' => 1,
                'is_uncertain' => 1,
                'month_no' => 'Lorem ipsum dolor sit amet',
                'year_id' => 1,
                'dynasty_id' => 1,
                'ruler_id' => 1,
                'absolute_year' => 'Lorem ipsum d'
            ],
        ];
        parent::init();
    }
}
