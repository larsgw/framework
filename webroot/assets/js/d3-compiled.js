// Make Visualizations responsive with respect to screen sizes
function responsivefy(svg) {
    // Get aspect ratio of (container + SVG) 
    var container = d3.select(svg.node().parentNode);
    var width = parseInt(svg.style("width"));
    var height = parseInt(svg.style("height"));
    var aspect = width / height;

    // Add viewBox and preserveAspectRatio properties to SVG,
    // and call resize so that SVG resizes on inital page load
    svg.attr("viewBox", "0 0 " + width + " " + height)
        .attr("perserveAspectRatio", "xMinYMid")
        .call(resize);

    // To register multiple listeners for same event type
    // Necessary if this function is invoked for multiple SVGs
    d3.select(window).on("resize." + container.attr("id"), resize);

    // Get width of container and resize SVG to fit into it
    function resize() {
        var targetWidth = parseInt(container.style("width"));
        svg.attr("width", targetWidth);
        svg.attr("height", Math.round(targetWidth / aspect));
    }
}

function barChart(barChartData) {
    // Set default margins, width and height
    var margin = {left: 100, top: 10, right: 10, bottom: 100}
    var width = 800 - margin.left - margin.right
    var height = 600 - margin.top - margin.bottom

    // Add SVG to bar chart container
    var svg = d3.select('#bar-chart')
                .append('svg')
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', height + margin.top + margin.bottom)
                    .call(responsivefy);

    // Add Group element to the SVG and translate it to center
    var g = svg.append('g')
                .attr('transform', 'translate(' + margin.left + ', ' + margin.top + ')')

    // Add X-label to Bar Chart
    g.append('text')
        .attr('class', 'x-axis-label')
        .attr('x', width/2)
        .attr('y', height + 95)
        .attr('font-size', '20px')
        .attr('text-anchor', 'middle')
        .text('Language')

    // Add Y-label to Bar Chart
    g.append('text')
        .attr('class', 'y-axis-label')
        .attr('x', -height/2)
        .attr('y', -60)
        .attr('font-size', '20px')
        .attr('text-anchor', 'middle')
        .attr('transform', 'rotate(-90)')
        .text('Artifacts Count')


    // Make all count values in the data integers from string(default)
    barChartData.forEach(function(data) {
        data.count = +data.count
    }) 

    // Create Band and Logarithmic scales (to scale X and Y-axes values accordingly)
    var x = d3.scaleBand()
        .domain(barChartData.map(function(d) {
            return d.language
        }))
        .range([0, width])
        .paddingOuter(0.3)
        .paddingInner(0.3)

    var y = d3.scaleLog()
        .domain([1, d3.max(barChartData, function(d) {
            return d.count
        })])
        .range([height, 0])
        .base(2);


    // Add X and Y-axes to the Bar Chart
    var xAxis = d3.axisBottom(x)
    g.append('g')
        .attr('class', 'x-axis')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis)
        .selectAll('text')
            .attr('y', 10)
            .attr('x', -5)
            .attr('text-anchor', 'end')
            .attr('transform', 'rotate(-30)');

    var yAxis = d3.axisLeft(y)
    g.append('g')
        .attr('class', 'y-axis')
        .call(yAxis)

    // Add the tooltip
    var tooltip = d3.select("body")
        .append("div")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("visibility", "hidden")
        .style("background", "#000")
        .style("color", "#FFF")
        .style("padding", "6px")
        .style("white-space", "pre-wrap")
        .text("");

    // Add data and display the Bar chart
    var chart = g.selectAll('rect')
        .data(barChartData)
        .enter()
        .append('rect')
            .attr('x', function(d) {
                return x(d.language)
            })
            .attr('y', function(d) {
                return y(d.count) 
            })
            .attr('width', x.bandwidth())
            .attr('height', function(d) {
                return height - y(d.count)
            })
            .attr('fill', '#4292c6')
            .on("mouseover", function(d) {
                tooltip.text("Language : " + d.language + "\n" + "Artifacts Count : " + d.count);
                return tooltip.style("visibility", "visible");
            })
            .on("mousemove", function() {
                return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
            })
            .on("mouseout", function() {
                return tooltip.style("visibility", "hidden");
            });
}

function donutChart(donutChartData) {
    // Set default margins, radius, width and height
    var margin = {left: 100, top: 10, right: 10, bottom: 100}
    var width = 800 - margin.left - margin.right
    var height = 600 - margin.top - margin.bottom
    var radius = Math.min(width, height) / 2

    // Add SVG to donut chart container
    var svg = d3.select('#donut-chart')
                .append('svg')
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', height + margin.top + margin.bottom)
                    .call(responsivefy);

    // Add Group element to the SVG and translate it to center 
    var g = svg.append('g')
                .attr('transform', 'translate(' + width/2 + ', ' + height/2 + ')')

    // Create a specific color palette for the data
    var color = d3.scaleOrdinal()
        .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

    // Make all count values in the data integers from string(default)
    donutChartData.forEach(function(data) {
        data.count = +data.count
    })

    // Add Legend to the Donut Chart
    var languages = donutChartData.map(function(d) {
        return d.language
    })

    var legend = g.append('g')
        .attr('transform', 'translate(' + (width - 300) + ', ' + (0) + ')')

    languages.forEach(function(language, i) {
        var legendRow = legend.append('g')
            .attr('transform', 'translate(0, ' + (i*20) + ')')

        legendRow.append('rect')
            .attr('height', 10)
            .attr('width', 10)
            .attr('fill', color(language))

        legendRow.append('text')
            .attr('x', -10)
            .attr('y', 10)
            .attr('text-anchor', 'end')
            .style('text-transform', 'capitalize')
            .text(language)
    })

    // Set arc properties of the Donut Chart
    var path = d3.arc()
        .outerRadius(radius - 10)
        .innerRadius(radius - 70);

    var pie = d3.pie()
        .value(function (d) {
            return d.count;
        });

    // Add the tooltip
    var tooltip = d3.select("body")
        .append("div")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("visibility", "hidden")
        .style("background", "#000")
        .style("color", "#FFF")
        .style("padding", "6px")
        .style("white-space", "pre-wrap")
        .text("");
    
    // Add data and display the Donut Chart
    var arc = g.selectAll(".arc")
        .data(pie(donutChartData))
        .enter()
        .append("g")
            .attr("class", "arc");

    arc.append("path")
        .attr("d", path)
        .style("fill", function (d) {
            return color(d.data.language);
        })
        .on("mouseover", function(d) {
            tooltip.text("Language : " + d.data.language + "\n" + "Artifacts Count : " + d.data.count);
            return tooltip.style("visibility", "visible");
        })
        .on("mousemove", function() {
            return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
        })
        .on("mouseout", function() {
            return tooltip.style("visibility", "hidden");
        });
}

function radarChart(radarChartData) {
    // Set default margins, width and height
    var margin = {left: 100, top: 100, right: 100, bottom: 100};
    var width = Math.min(800, window.innerWidth - 10) - margin.left - margin.right;
    var height = Math.min(800, window.innerHeight - margin.top - margin.bottom - 20);

    // Set the attributes of the Radar Chart
    var controls = {
        w: width,			    //Width of the circle
        h: height,				//Height of the circle
        margin: margin,         //The margins of the SVG
        translateX: (width/2 + margin.left),  //Translate in X-direction
        translateY: (height/2 + margin.top),   //Translate in Y-direction
        levels: 5,				//How many levels or inner circles should there be drawn
        maxValue: 0.5,          //What is the value that the biggest circle will represent (Random value, calculated later)
        labelFactor: 2,         //How much farther than the radius of the outer circle should the labels be placed
        wrapWidth: 60, 		    //The number of pixels after which a label needs to be given a new line
        opacityArea: 0.35, 	    //The opacity of the area of the blob
        dotRadius: 4, 			//The size of the colored circles of each blog
        opacityCircles: 0.1, 	//The opacity of the circles of each blob
        strokeWidth: 2, 		//The width of the stroke around each blob
        roundStrokes: true,	    //If true the area and stroke will follow a round path (cardinal-closed)
        color: d3.scaleOrdinal(d3.schemeCategory10)	//Color function
    };

    // Add SVG to the Radar Chart container
    var svg = d3.select("#radar-chart")
        .append("svg")
            .attr("width",  controls.w + controls.margin.left + controls.margin.right)
            .attr("height", controls.h + controls.margin.top + controls.margin.bottom)
            .call(responsivefy);

    // Append Group element to the SVG and translate it to center		
    var g = svg.append("g")
        .attr("transform", "translate(" + controls.translateX + "," + controls.translateY + ")");


    // Initialize data and the legend
    var data = radarChartData.results
    var legendOptions = radarChartData.legend
    
    // Calculate the maximum value required to be represented in the Radar Chart
    // If the supplied maxValue is smaller than the actual one, replace it by the max in the data
    var maxValue = Math.max(controls.maxValue, d3.max(data, function(i) {
        return d3.max(i.map(function(o) {
            return o.value;
        }))
    }));

    // Get names of all the surrounding axis
    var allAxis = (data[0].map(function(i, j) {
        return i.axis
    }));

    var total = allAxis.length					         //Number of different axes
    var radius = Math.min(controls.w/2, controls.h/2) 	 //Radius of the outermost circle
    var angleSlice = Math.PI * 2 / total;		         //Width in radians of each "slice"

    // Create Scale for the radius
    var rScale = d3.scaleLog()
        .domain([1, maxValue])
        .range([0, radius])
        .base(2);
    
    
    // Add Legend
    // Legend Options are taken from backend directly
    // Create the title for the legend
    var text = g.append("text")
        .attr("class", "title")
        .attr("transform", "translate(" + (-controls.translateX + 90) + "," + (-controls.translateY) + ")") 
        .attr("x", width - 70)
        .attr("y", 10)
        .attr("font-size", "12px")
        .attr("fill", "#404040")
        .text("Regions")
            .style('font-weight', 'bold');

    // Initiate Legend	
    var legend = g.append("g")
        .attr("class", "legend")
        .attr("height", 100)
        .attr("width", 200)
        .attr("transform", "translate(" + (-controls.translateX + 85) + "," + (-controls.translateY + 25) + ")");

    // Create colour squares to label legend options
    legend.selectAll('rect')
        .data(legendOptions)
        .enter()
        .append("rect")
            .attr("x", width - 65)
            .attr("y", function(d, i) { return i * 20; })
            .attr("width", 10)
            .attr("height", 10)
            .style("fill", function(d, i) { return controls.color(i); });

    // Add text next to squares
    legend.selectAll('text')
        .data(legendOptions)
        .enter()
        .append("text")
            .attr("x", width - 52)
            .attr("y", function(d, i) { return i * 20 + 9; })
            .attr("font-size", "11px")
            .attr("fill", "#737373")
            .text(function(d) { return d; });	


    // Glow Filter
    // Create Filter for the outside glow
    var filter = g.append('defs')
        .append('filter')
        .attr('id', 'glow');

    var feGaussianBlur = filter.append('feGaussianBlur')
        .attr('stdDeviation', '2.5')
        .attr('result', 'coloredBlur');

    var feMerge = filter.append('feMerge');

    var feMergeNode_1 = feMerge.append('feMergeNode')
        .attr('in', 'coloredBlur');

    var feMergeNode_2 = feMerge.append('feMergeNode')
        .attr('in', 'SourceGraphic');


    // Draw the circular grid
    // Create Wrapper for the grid & axes
    var axisGrid = g.append("g")
        .attr("class", "axisWrapper");

    // Draw the background circles
    axisGrid.selectAll(".levels")
        .data(d3.range(1,(controls.levels+1)).reverse())
        .enter()
        .append("circle")
            .attr("class", "gridCircle")
            .attr("r", function(d, i) { return radius/controls.levels * d; })
            .style("fill", "#CDCDCD")
            .style("stroke", "#CDCDCD")
            .style("fill-opacity", controls.opacityCircles)
            .style("filter" , "url(#glow)");

    // Add Text indicating at what number each level is present
    axisGrid.selectAll(".axisLabel")
        .data(d3.range(1,(controls.levels+1)).reverse())
        .enter()
        .append("text")
            .attr("class", "axisLabel")
            .attr("x", 4)
            .attr("y", function(d) { return -d * radius/controls.levels; })
            .attr("dy", "0.4em")
            .style("font-size", "10px")
            .attr("fill", "#737373")
            .text(function(d, i) { return Math.round(rScale.invert(radius * d/controls.levels)); });


    // Draw the axes
    // Create straight lines radiating outward from the center
    var axis = axisGrid.selectAll(".axis")
        .data(allAxis)
        .enter()
        .append("g")
           .attr("class", "axis");

    // Append lines
    axis.append("line")
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", function(d, i){ return rScale(maxValue*1.1) * Math.cos(angleSlice * i - Math.PI/2); })
        .attr("y2", function(d, i){ return rScale(maxValue*1.1) * Math.sin(angleSlice * i - Math.PI/2); })
        .attr("class", "line")
        .style("stroke", "white")
        .style("stroke-width", "2px");

    // Append the labels at each axis
    axis.append("text")
        .attr("class", "legend")
        .style("font-size", "11px")
        .attr("text-anchor", "middle")
        .attr("dy", "0.35em")
        .attr("x", function(d, i) { 
            return rScale(maxValue * controls.labelFactor) * Math.cos(angleSlice * i - Math.PI/2); 
        })
        .attr("y", function(d, i) { 
            return rScale(maxValue * controls.labelFactor) * Math.sin(angleSlice * i - Math.PI/2); 
        })
        .text(function(d) { 
            return d; 
        })
        .call(wrap, controls.wrapWidth);


    // Draw the radar chart blobs
    // Define radial line function
    var radarLine = d3.lineRadial()
        .curve(d3.curveBasisClosed)
        .radius(function(d) { return rScale(d.value); })
        .angle(function(d,i) {	return i * angleSlice; });

    if (controls.roundStrokes) {
        radarLine.curve(d3.curveCardinalClosed);
    }

    // Create a wrapper for the blobs	
    var blobWrapper = g.selectAll(".radarWrapper")
        .data(data)
        .enter()
        .append("g")
           .attr("class", "radarWrapper");

    // Append backgrounds	
    blobWrapper.append("path")
        .attr("class", "radarArea")
        .attr("d", function(d, i) { return radarLine(d); })
        .style("fill", function(d, i) { return controls.color(i); })
        .style("fill-opacity", controls.opacityArea)
        .on('mouseover', function (d, i) {
            // Dim all blobs
            d3.selectAll(".radarArea")
                .transition()
                    .duration(200)
                .style("fill-opacity", 0.1); 

            // Bring back the hovered over blob
            d3.select(this)
                .transition()
                    .duration(200)
                .style("fill-opacity", 0.7);	
        })
        .on('mouseout', function() {
            // Bring back all blobs
            d3.selectAll(".radarArea")
                .transition()
                    .duration(200)
                .style("fill-opacity", controls.opacityArea);
        });

    // Create the outlines	
    blobWrapper.append("path")
        .attr("class", "radarStroke")
        .attr("d", function(d, i) { return radarLine(d); })
        .style("stroke-width", controls.strokeWidth + "px")
        .style("stroke", function(d, i) { return controls.color(i); })
        .style("fill", "none")
        .style("filter", "url(#glow)");		

    // Append the circles
    blobWrapper.selectAll(".radarCircle")
        .data(function(d, i) { return d; })
        .enter()
        .append("circle")
            .attr("class", "radarCircle")
            .attr("r", controls.dotRadius)
            .attr("cx", function(d, i) { return rScale(d.value) * Math.cos(angleSlice * i - Math.PI/2); })
            .attr("cy", function(d, i) { return rScale(d.value) * Math.sin(angleSlice * i - Math.PI/2); })
            .style("fill", function(d, i, j) { return controls.color(j); })
            .style("fill-opacity", 0.8);


    // Append invisible circles for tooltip
    // Create Wrapper for invisible circles on top
    var blobCircleWrapper = g.selectAll(".radarCircleWrapper")
        .data(data)
        .enter()
        .append("g")
            .attr("class", "radarCircleWrapper");

    // Append a set of invisible circles on top for the mouseover pop-up
    blobCircleWrapper.selectAll(".radarInvisibleCircle")
        .data(function(d, i) { return d; })
        .enter()
        .append("circle")
            .attr("class", "radarInvisibleCircle")
            .attr("r", controls.dotRadius * 1.5)
            .attr("cx", function(d, i){ return rScale(d.value) * Math.cos(angleSlice * i - Math.PI/2); })
            .attr("cy", function(d, i){ return rScale(d.value) * Math.sin(angleSlice * i - Math.PI/2); })
            .style("fill", "none")
            .style("pointer-events", "all")
            .on("mouseover", function(d, i) {
                newX = parseFloat(d3.select(this).attr('cx')) - 10;
                newY = parseFloat(d3.select(this).attr('cy')) - 10;

                tooltip
                    .attr('x', newX)
                    .attr('y', newY)
                    .text(d.value)
                    .transition()
                        .duration(200)
                    .style('opacity', 1);
            })
            .on("mouseout", function(){
                tooltip
                    .transition()
                        .duration(200)
                    .style("opacity", 0);
            });

    // Set up the small tooltip to display when circle is hovered
    var tooltip = g.append("text")
        .attr("class", "tooltip")
        .style("opacity", 0);


    // Helper Function
    // Wraps SVG text	
    function wrap(text, width) {
        text.each(function() {
            var text = d3.select(this);
            var words = text.text().split(/\s+/).reverse();
            var word;
            var line = [];
            var lineNumber = 0;
            var lineHeight = 1.4; // in em
            var y = text.attr("y");
            var x = text.attr("x");
            var dy = parseFloat(text.attr("dy"));
            var tspan = text.text(null)
                .append("tspan")
                    .attr("x", x)
                    .attr("y", y)
                    .attr("dy", dy + "em");

            while (word = words.pop()) {
                line.push(word);
                tspan.text(line.join(" "));

                if (tspan.node().getComputedTextLength() > width) {
                    line.pop();
                    tspan.text(line.join(" "));
                    line = [word];
                    tspan = text.append("tspan")
                        .attr("x", x)
                        .attr("y", y)
                        .attr("dy", ++lineNumber * lineHeight + dy + "em")
                        .text(word);
                }
            }
        });
    }
}

function lineChart(lineChartData) {
    // Set default margins, width and height
    var margin = {top: 10, right: 30, bottom: 200, left: 100};
    var width = 800 - margin.left - margin.right;
    var height = 600 - margin.top - margin.bottom;

    // Add SVG to line chart container and append a group to the same
    var svg = d3.select("#line-chart")
        .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .call(responsivefy)
        .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    // Format data
    var categories = [];
    // For all types of key except period(x-axis variable), format them
    for (var key in lineChartData[0]) {
         if (key != 'period') {
             var obj = {
                 category: key,  // categorical variable
                 values: lineChartData.map(function(d) {
                     return {period: d.period, yValue: d[key]};
                 })
             };
             categories.push(obj);
         }
    }


    // Define X-scale
    var xScale = d3.scaleBand()
        .domain(lineChartData.map(function(d) {
            return d.period;
        }))
        .range([ 0, width ])
        .paddingInner(1.0)
        .paddingOuter(0.2);

    // Append X-axis to the Line Chart
    xAxis = svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(xScale));

    // Rotate labels separately
    xAxis.selectAll('text')
        .attr('y', 10)
        .attr('x', -5)
        .attr('text-anchor', 'end')
        .attr('transform', 'rotate(-50)');

    // Custom Invert function for Band Scale
    function scaleBandInvert(scale) {
        var domain = scale.domain();
        var paddingOuter = scale(domain[0]);
        var eachBand = scale.step();

        return function (value) {
            var index = Math.floor(((value - paddingOuter) / eachBand));
            return domain[Math.max(0,Math.min(index, domain.length-1))];
        }
    }

    // Define Y-scale
    var yScale = d3.scaleLinear()
        .domain([
            d3.min(categories, function(c) { return d3.min(c.values, function(d) { return d.yValue; }); }),
            d3.max(categories, function(c) { return d3.max(c.values, function(d) { return d.yValue; }); })
        ])
        .range([ height, 0 ]);

    // Append Y-axis to the Line Chart
    yAxis = svg.append("g")
        .call(d3.axisLeft(yScale))
        .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", "0.71em")
            .attr("fill", "#000")
            .text("Number of Artifacts");


    // Define Color Scale
    var colorScale = d3.scaleOrdinal(d3.schemeCategory10)
        .domain(categories.map(function(c) { 
            return c.category; 
        }));


    // Add Legend to the Line Chart
    var legend = svg.append("g")
        .attr('transform', 'translate(' + width + ', 0)');

    var i = 0;
    for (var key in lineChartData[0]) {
         if (key != 'period') {
             var legendRow = legend.append('g')
                .attr('transform', 'translate(0, ' + (i*20) + ')');

            legendRow.append('rect')
                .attr('height', 10)
                .attr('width', 10)
                .attr('fill', colorScale(key));

            legendRow.append('text')
                .attr('x', -10)
                .attr('y', 10)
                .attr('text-anchor', 'end')
                .style('text-transform', 'capitalize')
                .text(key);

             i++;
         }
    }


    // Add a clipPath: everything out of this area won't be drawn
    var clip = svg.append("defs").append("svg:clipPath")
        .attr("id", "clip")
        .append("svg:rect")
        .attr("width", width )
        .attr("height", height )
        .attr("x", 0)
        .attr("y", 0);


    // Define brush restricted to X-axis
    var brush = d3.brushX()                   // Add the brush feature using the d3.brush function
        .extent( [ [0,0], [width,height] ] )  // Initialise the brush area: start at 0,0 and finishes at width,height: it means the whole graph area is selected
        .on("end", updateChart)               // Each time the brush selection changes, trigger the 'updateChart' function

    // Define Line
    var line = d3.line()
    //    .curve(d3.curveBasis)
        .x(function(d) { return xScale(d.period); })
        .y(function(d) { return yScale(d.yValue); });

    // Add Line categorized by categorical variable
    var category = svg.selectAll(".category")
        .data(categories)
        .enter().append("g")
            .attr("clip-path", "url(#clip)")
            .attr("class", "category");

    category.append("path")
        .attr("class", "line")
        .attr("fill", "none")
        .attr("d", function(d) { return line(d.values); })
        .style("stroke", function(d) { return colorScale(d.category); })
        .attr("stroke-width", 2.5);

    //// Append text after the last point
    //category.append("text")
    //    .datum(function(d) { 
    //        return {category: d.category, value: d.values[d.values.length - 1]}; 
    //    })
    //    .attr("transform", function(d) { return "translate(" + xScale(d.value.period) + "," + yScale(d.value.yValue) + ")"; })
    //    .attr("x", 3)
    //    .attr("dy", "0.35em")
    //    .style("font", "10px sans-serif")
    //    .text(function(d) { return d.category; });

    // Add the brushing to the SVG
    svg.append("g")
        .attr("class", "brush")
            .call(brush);


    // Update the chart for given boundaries
    function updateChart() {
        // Get the selected boundaries
        extent = d3.event.selection

        var start = scaleBandInvert(xScale)(extent[0]);
        var end = scaleBandInvert(xScale)(extent[1]);
        var count = false;

        var filteredData = [];
        lineChartData.forEach(function(d) {
            if (d.period == start)
                count = true;

            if (count)
                filteredData.push(d);

            if (d.period == end)
                count = false;                
        });

        xScale.domain(filteredData.map(function(d) {
            return d.period;
        }))

        // Remove the grey brush area as soon as the selection has been done
        category.select(".brush").call(brush.move, null);

        // Update axis and line position
        xAxis.transition().duration(1000).call(d3.axisBottom(xScale));

        // Format filteredData
        var filteredCategories = [];
        // For all types of key except period(x-axis variable), format them
        for (var key in filteredData[0]) {
             if (key != 'period') {
                 var obj = {
                     category: key,  // categorical variable
                     values: filteredData.map(function(d) {
                         return {period: d.period, yValue: d[key]};
                     })
                 };
                 filteredCategories.push(obj);
             }
        }

        category.data(filteredCategories)
            .select('.line')
            .transition()
            .duration(1000)
            .attr("d", function(d) { return line(d.values); })
            .style("stroke", function(d) { return colorScale(d.category); });
    }

    // Reinitialize the chart on double click
    svg.on("dblclick", function() {
        xScale.domain(lineChartData.map(function(d) {
            return d.period;
        }))

        xAxis.transition()
            .call(d3.axisBottom(xScale))
            .selectAll('text')
                .attr('y', 10)
                .attr('x', -5)
                .attr('text-anchor', 'end')
                .attr('transform', 'rotate(-50)');

        category.data(categories)
            .select('.line')
            .transition()
            .attr("d", function(d) { return line(d.values); })
            .style("stroke", function(d) { return colorScale(d.category); });
    });
}

function dendrogramChart(dendrogramChartData) {
    // Set default margins, width and height
    var margin = {top: 0, right: 0, bottom: 10, left: 40};
    var width = 1200 - margin.left - margin.right;
    var height = 2000 - margin.top - margin.bottom;

    // Add SVG to dendrogram chart container
    var svg = d3.select("#dendrogram-chart")
        .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .call(responsivefy);

    // Add Group element to the SVG and translate it to center
    var g = svg.append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    // Get all the non-leaf node names
    var parents  = new Set();
    dendrogramChartData.forEach(function(d) {
        parents.add(d.parent);
    })

    // Get max of leaf values for X-axis domain
    var max = 0;
    dendrogramChartData.forEach(function(d) {
        if (!parents.has(d.material))
            max = Math.max(max, d.count)
    })

    // Define X-scale
    var xScale =  d3.scaleLinear()
        .domain([0, max])
        .range([0, 700]);   // Max value of range must be less than size of mapped bar chart

    // Define X-axis
    var xAxis = d3.axisTop()
        .scale(xScale)

    // Add Color Scale
    var colorScale = d3.scaleOrdinal(d3.schemeCategory10)
        .domain(dendrogramChartData.map(function(d) { 
            return d.material; 
        }));


    // Set up a way to handle the data
    var tree = d3.cluster()                     // This D3 API method setup the Dendrogram datum position.
        .size([height, width - 760])            // Total width - bar chart width = Dendrogram chart width
        .separation(function separate(a, b) {
            return a.parent == b.parent         // 2 levels tree grouping for category
                || a.parent.parent == b.parent
                || a.parent == b.parent.parent ? 0.4 : 0.8;
            });

    // Make all count values in the data integers from string(default)
    dendrogramChartData.forEach(function(d) {
        d.count = +d.count;
    })

    
    // Format data in tree form
    var root = d3.stratify()
        .id(function(d) { return d.material; })
        .parentId(function(d) { return d.parent; })
        (dendrogramChartData);

    tree(root);

    // Draw a line connecting every datum to its parent
    var link = g.selectAll(".link")
        .data(root.descendants().slice(1))
        .enter().append("path")
            .attr("class", "link")
            .attr("d", function(d) {
                return "M" + d.y + "," + d.x
                        + "C" + (d.parent.y + 100) + "," + d.x
                        + " " + (d.parent.y + 100) + "," + d.parent.x
                        + " " + d.parent.y + "," + d.parent.x;
            });

    // Setup position for every datum
    // Apply different CSS classes to parents and leafs
    var node = g.selectAll(".node")
        .data(root.descendants())
        .enter().append("g")
            .attr("class", function(d) { return "node" + (d.children ? " node--internal" : " node--leaf"); })
            .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

    // Draw a small circle for every datum
    node.append("circle")
        .attr("r", 4);

    // Setup G for every leaf datum
    var leafNodeG = g.selectAll(".node--leaf")
        .append("g")
            .attr("class", "node--leaf-g")
            .attr("transform", "translate(" + 8 + "," + -13 + ")");

    leafNodeG.append("rect")
        .attr("class", "shadow")
        .style("fill", function (d) {return colorScale(d.data.material);})
        .attr("width", 2)
        .attr("height", 30)
        .attr("rx", 2)
        .attr("ry", 2)
        .transition()
            .duration(800)
            .attr("width", function (d) {return xScale(d.data.count);});

    leafNodeG.append("text")
        .attr("dy", 19.5)
        .attr("x", 8)
        .style("text-anchor", "start")
        .text(function (d) {
            return d.data.material;
        });

    // Write down text for every parent datum
    var internalNode = g.selectAll(".node--internal");
    internalNode.append("text")
        .attr("y", -10)
        .style("text-anchor", "middle")
        .text(function (d) {
            return d.data.material;
        });

    // Attach axis on top of the first leaf datum
    // Get first leaf node
    var firstEndNode = g.select(".node--leaf");
    var minYTransform = firstEndNode.attr('transform').split(',')[1].split(')')[0];
    $('.node--leaf').each(function() {
        var currentNode = d3.select(this);
        currentYTransform = currentNode.attr('transform').split(',')[1].split(')')[0];
        currentYTransform = +currentYTransform

        if (currentYTransform < minYTransform) {
            minYTransform = currentYTransform;
            firstEndNode = currentNode;
        }
    });

    firstEndNode.insert("g")
        .attr("class", "xAxis")
        .attr("transform", "translate(" + 7 + "," + -14 + ")")
        .call(xAxis);

    // Tick mark for X-axis
    firstEndNode.insert("g")
        .attr("class", "grid")
        .attr("transform", "translate(7," + (height - 15) + ")")
        .call(d3.axisBottom()
            .scale(xScale)
            .ticks(5)
            .tickSize(-height, 0, 0)
            .tickFormat("")
        );

    // Emphasize the Y-axis baseline
    svg.selectAll(".grid").select("line")
        .style("stroke-dasharray", "20,1")
        .style("stroke", "black");

    // Moving ball (initially place it outside of visibility area)
    var ballG = svg.insert("g")
        .attr("class", "ballG")
        .attr("transform", "translate(" + (width+200) + "," + height/2 + ")");

    ballG.insert("circle")
        .attr("class", "shadow")
        .style("fill", "steelblue")
        .attr("r", 5);

    ballG.insert("text")
        .style("text-anchor", "middle")
        .attr("dy", 5)
        .text("0.0");

    // Animation functions for mouse on and off events
    d3.selectAll(".node--leaf-g")
        .on("mouseover", handleMouseOver)
        .on("mouseout", handleMouseOut);

    // Helper Mouse event handlers
    function handleMouseOver(d) {
        var leafG = d3.select(this);

        leafG.select("rect")
            .attr("stroke", "#4D4D4D")
            .attr("stroke-width", "2");

        var ballGMovement = ballG.transition()
            .duration(400)
            .attr("transform", "translate(" + (d.y
                    + xScale(d.data.count) + 75) + ","
                    + (d.x + 1.5) + ")");

        ballGMovement.select("circle")
            .style("fill", colorScale(d.data.material))
            .attr("r", 18);

        ballGMovement.select("text")
            .delay(300)
            .text(d.data.count);
    }

    function handleMouseOut() {
        var leafG = d3.select(this);

        leafG.select("rect")
            .attr("stroke-width", "0");
    }
}

function choroplethMap(choroplethMapData, choroplethMapType) {
    // Set default margins, width and height
    var margin = {top: 50, right: 50, bottom: 50, left: 50};
    var width = 1200 - margin.left - margin.right;
    var height = 800 - margin.top - margin.bottom;

    // Define Zoom event
    var zoom = d3.zoom()
        .scaleExtent([1, 10])
        .on("zoom", zoomed)

    // Add SVG to Choropleth map container
    var svg = d3.select("#choropleth-map")
        .append("svg");
    
    // Append a group to the SVG and call zoom event on it
    var canvas = svg
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .call(responsivefy)
        .call(zoom)
        .call(zoom.transform, d3.zoomIdentity.translate(-1700, -800).scale(4))
            .append("g")
                .attr("transform","translate(-1700, -800) scale(4, 4)");

    // Helper function for Zoom
    function zoomed() {
        if (canvas)
            canvas.attr("transform", d3.event.transform);
    }

    
    // Define Map and projection
    var path = d3.geoPath();
    var projection = d3.geoMercator()
      .scale(100)
      .center([0,20])
      .translate([width / 2, height / 2]);

    // Define color scale
    var colorScale = d3.scaleOrdinal(d3.schemeBlues[7])

    // Add custom controls to the map
    d3.select("#reset").on("click", function() {
        svg.transition().duration(500).call(zoom.transform, d3.zoomIdentity);
    });

    d3.select("#zoom-in").on("click", function() {
        zoom.scaleBy(svg.transition().duration(500), 1.2);
    }); 

    d3.select("#zoom-out").on("click", function() {
        zoom.scaleBy(svg.transition().duration(500), 0.8);
    });
    
    // Define tooltip to display information about the region/proveniences
    var tooltip = d3.select("body")
        .append("div")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("visibility", "hidden")
        .style("background", "#000")
        .style("color", "#FFF")
        .style("padding", "6px")
        .style("white-space", "pre-wrap")
        .text("");
    
    
    // Helper to draw map for proveniences
    function choroplethMapByProveniences(data) {
        // Draw the map
        canvas.append("g")
            .selectAll("circle")
            .data(data.features)
            .enter()
            .append("circle")
                // Plot each Provenience
                .attr("cx", function (d) { return projection(d.geometry.coordinates)[0]; })
                .attr("cy", function (d) { return projection(d.geometry.coordinates)[1]; })
        		.attr("r", "0.5px")
                // Set the color of each Provenience
                .attr("fill", function (d) {
                    return colorScale(d.properties.region)
                })
                .on("mouseover", function(d) {
                    // Add tooltip to be displayed when any provenience is hovered
                    tooltip.text("Provenience : " + d.properties.provenience + "\n" + "Region : " + d.properties.region);
                    return tooltip.style("visibility", "visible");
                })
                .on("mousemove", function() {
                    return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
                })
                .on("mouseout", function() {
                    return tooltip.style("visibility", "hidden");
                });
    }
    
    
    // Helper to draw map for regions
    function choroplethMapByRegions(data) {
        // Draw the map
        canvas.append("g")
            .selectAll("path")
            .data(data.features)
            .enter()
            .append("path")
                // Draw each Region
                .attr("d", d3.geoPath()
                    .projection(projection)
                )
                // Set the color of each Region
                .attr("fill", function (d) {
                    return colorScale(d.properties.region)
                })
                .on("mouseover", function(d) {
                    // Add tooltip to be displayed when any region is hovered
                    tooltip.text("Region : " + d.properties.region);
                    return tooltip.style("visibility", "visible");
                })
                .on("mousemove", function() {
                    return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
                })
                .on("mouseout", function() {
                    return tooltip.style("visibility", "hidden");
                });   
    }

    d3.json("assets/map/world.geojson").then(function(backgroundMap) {
        // Draw the background World map
        canvas.append("g")
            .selectAll("path")
            .data(backgroundMap.features)
            .enter()
            .append("path")
                // Draw each country
                .attr("d", d3.geoPath()
                    .projection(projection)
                )
                // Set the color of each country
                .attr("fill", "#EAEAEA")

        // Plot the regions/proveniences after the background map is loaded
        if (choroplethMapType == "proveniences")
            choroplethMapByProveniences(choroplethMapData);
        else
            choroplethMapByRegions(choroplethMapData);
    })
}