# CDLI Framework

[![pipeline status](https://gitlab.com/cdli/framework/badges/phoenix/develop/pipeline.svg)](https://gitlab.com/cdli/framework/-/commits/phoenix/develop)

**NOTE:** This branch is for Phoenix (aka "Framework 2.0") development only. For the current "1.0" codebase, see the mainline [master](https://gitlab.com/cdli/framework/tree/master) or [develop](https://gitlab.com/cdli/framework/tree/develop) branches of this repository.

Instructions to install the framework for local development are in [FRAMEWORK_INSTALL.md](FRAMEWORK_INSTALL.md). Front-end developer's README is in [framework/dev](dev/README.md).

## Repository layout

The following is a non-exhaustive list of folders explaining the general layout strategy of the repository:

* `/` : Repo root *(mounted as '/srv' in container filesystem)*
* `/app` : Web applications and tools *(live code, served at specified routes)*
    * `/common` : Common/shared application files
  	* `/brat` : Annotation GUI server
	* `/cake` : CDLI Framerwork's new heart
    * `/tools` : Contains helper Python tools like "upload"
* `/dev` : CLI/scripts for development environment.
* `/private` : Private static files
    * `/logs` : logs directory for cake
* `/webroot` : Public static files *(served by webserver as default `/`)*
    * `/assets` : web assets *(`css/`, `js/`, etc.)*
    * `/bulk_data` : CDLI bulk data *(stub)*
    * `/dl` : CDLI artifact images *(stub)*
    * `/files` : other static assets
    * `/images` : Static image assets
    * `/pubs` : Static CDLI publication documents/PDFS


## Submodules

There are submodules in this repository and will be cloned as empty directories. 
To populate these submodules, you need to initialize and update them with the command:

`git submodule update --init --recursive`


## Containerization

* Every app in the `/app` folder will run in its own Docker container.
* Each app container will follow the same filesystem layout, but internally will only contain/have access to appropriate relevant folders (i.e. every container should see at least `/app/common/` and `/app/[myapp]/`).

* Since the user-facing webserver container (nginx) handles static assets, most containers should not even need to see `/webroot`.

* Developers can work on individual apps on their local machines using the provided Development CLI. Using Docker Compose, this provisions a virtual set of infrastructure containers (nginx, mariadb, etc) that mirror the configuration of the live servers, while allowing users to modify app source files directly without building container images. More info is available in the [CLI's README](dev/README.md).


## Contribution & Development

* Since all the apps will share one repository, it is crucial that developers employ good Git practices.
* The Git workflow that this project will adhere to (more or less) is called *Git Flow*. A longer explanation of it (with charts!) is available [here](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). The basic summary with respect to the Phoenix/Version 2 branches is:
  * The `phoenix/develop` branch is for merging feature branches and applying fixes. It may not always be stable, but should aim for "Beta" quality stability. Significant new development should never occur directly in the `phoenix/develop` branch.
  * Development on individual apps, features, or parts of the repository should take place in a branch of `phoenix/develop` named `phoenix/feature/[app]` (or `phoenix/feature/[app]_[description]`). These branches are unprotected and can be modified by anyone with "Developer" access to the repository.
  * Once a `feature/` branch is working, a pull request (aka "merge request") can be made to merge it back into the `phoenix/develop` branch.
  * Since the `phoenix/*` branches are meant for the development of a large-scale update to the Framework codebase and apps, _there is no `phoenix/master` branch_. When ready for release, the `phoenix/develop` code will be merged into the mainline (`develop` and `master`) branches of the repository.
* If multiple developers are working on a single app, it is important to coordinate to ensure that they are working in different sub-branches and that their changes may be merged with one another cleanly.
* Only people with "Master" repository access will be able merge/push to the `phoenix/develop` branch. They will handle merging in updates from the app branches.


## License & Copyright

Non-software components (including publications, collections, and associated data) are copyright their respective owners and no license is implied except where provided in the relevant folders. Unless otherwise noted, all original code and documentation is licensed under the [MIT License](LICENSE.txt).
